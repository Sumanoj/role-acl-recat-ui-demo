
import {ADD_ITEM} from './AddActionType'
import {DELETE_ITEM} from './AddActionType'

export function addItem(value){
    return{
            type: ADD_ITEM,
            task: value
    }
}

export function deleteItem(value){
    return{
        type: DELETE_ITEM,
        task: value

    }
}
