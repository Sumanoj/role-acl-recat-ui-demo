
import firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyCTC8Q_HAmb90zlnlcRIwGt6H2GbMeJtHE",
    authDomain: "admin-4a519.firebaseapp.com",
    databaseURL: "https://admin-4a519.firebaseio.com",
    projectId: "admin-4a519",
    storageBucket: "admin-4a519.appspot.com",
    messagingSenderId: "317200609802",
    appId: "1:317200609802:web:8eb34d0acb1e408c6025f3",
    measurementId: "G-JZSJS3JXK1"
  };
  firebase.initializeApp(firebaseConfig);
  export default firebase;
