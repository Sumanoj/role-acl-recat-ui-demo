import React from 'react';


class Basic extends React.Component{
    constructor(props){
        super(props);
        this.state = {name:"manoj"}
    }
    render(){
        return <Child1 name={this.state.name}>hello</Child1>
    }
}

    class Child1 extends React.Component{
        render(){
            return(
                <h1>your name is:{this.props.name}</h1>
            )
        }
    }
    
export default Basic;


