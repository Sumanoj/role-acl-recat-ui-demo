import React from 'react';

class Listing extends React.Component{
    constructor(props){
        super(props);
        this.state={
            arr:["hello","hi"]
        }
    }
    render(){
        return(
            <List1 a={this.state.arr}/>
        )
    }
}
class List1 extends React.Component{
    render(){
        return(
            <div>
                <ul>
                    {this.props.a.map(ele=><li>{ele}</li>)}
                </ul>
            </div>
        )
    } 
}
export default Listing;