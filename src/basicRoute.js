import React from 'react'
import {BrowserRouter as Router,Route,Switch,Link} from 'react-router-dom';

const Home=()=>{
    return <h1>welcome Home</h1>
}

const About=()=>{
    return <h1>Welcome About</h1>
}

const User=({match})=>{
  return <h1> Welcome User {match.params.username}</h1>
}

function Example(){
    return(
        <Router>
            <ul>
                <li>
                <Link to="/" exact >Home</Link>
                </li>
                <li>
                <Link to="/About">About</Link>
                </li>
                <li>
                <Link to={`/user/ffytg`} >User</Link>
                </li>

                </ul>
            
            
            <Switch>

            <Route exact path="/" component={Home}></Route>
            <Route path="/About" component={About}></Route>
            <Route path="/User/:username" component={User}></Route>

            </Switch>
        </Router>
    )
}
export default Example;