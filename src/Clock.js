import React from 'react'

function FormatedDate(props){
    return <h2>{props.date.toLocaleTimeString()}</h2>
}

 class Clock extends React.Component{
     constructor(props){
         super(props);
         this.state={date:new Date()}
     }
     componentDidMount(){
         this.timerID=setInterval(() => this.tick(),
         1000
             
         );
     }
     componentWillUnmount(){
         clearInterval(this.timerID);
     }
     tick(){
         this.setState({date:new Date()});
     }

     render(){
         return(
             <div>
                 <h1>hello World</h1>
                 <FormatedDate date={this.state.date}/>
             </div>
         )
     }
 }

 function App1(){
     return(
        <div>
        <Clock/>
        <Clock/>
        <Clock/>
   
        </div>
     )
    
 }
 export default App1;