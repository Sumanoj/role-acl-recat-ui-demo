import React from 'react';
import firebase from './../firebaseConfig'

class Admin extends React.Component{
    constructor(){
        super();
        this.state={
            input:'',
            userList:{},
            edit:false,
            title:''
        }   

    }
    handler = (e) =>{
        this.setState({input:e.target.value})
    }
    addSubmit=(e)=>{
        e.preventDefault();
        firebase.database().ref("Dev").push(this.state);
    }

    removeItem(itemId) {
        const itemRef = firebase.database().ref(`Dev/${itemId}`);
        itemRef.remove();
      }

      handleEdit = (e) => {
          this.setState({
              title:e.target.value
          })
      }

    getUserList=()=>{
        return Object.keys(this.state.userList).map((ele)=> 
        <div key={ele}>
            {console.log(ele)}
        {this.state.userList[ele].input} 
        <button onClick = {() => this.removeItem(ele)}>delete</button>
        <button onClick = {() => this.onEditHandle(ele,this.state.userList[ele].input)}>Edit</button>
        </div>)

    }
    renderEditForm() {  
          if (this.state.edit) {   
                 return <form onSubmit={(e)=>this.onUpdateHandle(e)}>   
                   <input type="text" name="updatedItem" className="item" value={this.state.title} onChange={(e)=>this.handleEdit(e)} />    
                       <button className="update-add-item">Update</button>    
          </form>   
           }
          }

          onEditHandle(id,title) { 
               this.setState({    edit: true,    id: id,    title: title  });
            }


            onUpdateHandle(event) { 
                 event.preventDefault();
                
                 let id=this.state.id
                 this.state.userList[id].input=this.state.title
                 firebase.database().ref(`Dev/${id}`).update(this.state.userList[id])
            this.setState({edit: false   });
        }
         



    componentDidMount() {
        const itemsRef = firebase.database().ref('Dev');
        itemsRef.on('value', (snapshot) => {
            debugger
          let items = snapshot.val();
          this.setState({
            userList: items
          });
        });
      }
    

    render(){
        return(
            <div>
            <form onSubmit={(e)=>this.addSubmit(e)}> 
                <input type="text" value={this.state.input} onChange={(e)=>this.handler(e)}/>
                <button>submit</button>
            </form>
            {this.getUserList()}
                {this.renderEditForm()}
            </div>
        )

    }
    
}

export default Admin;