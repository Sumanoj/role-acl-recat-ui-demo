import React from 'react';
import firebase from './../firebaseConfig'

class DeleteDeveloper extends React.Component{
    constructor(props){
        super(props);
        this.state={
            input:'',
        }   

    }
    handler = (e) =>{
        this.setState({input:e.target.value})
    }
    deleteSubmit=(e)=>{
        e.preventDefault();
        //firebase.database().ref("Dev").push(this.state);
    }
    removeItem(itemId) {
        const itemRef = firebase.database().ref(`/Dev/${itemId}`);
        itemRef.remove();
      }
      

    render(){
        return(
            <div>
            <form onSubmit={(e)=>this.deleteSubmit(e)}> 
                <input type="text" value={this.state.input} onChange={(e)=>this.handler(e)}/>
                <button onClick={() => this.removeItem(this.state.input)}>Remove Item</button>
            </form>
            </div>
        )

    }
    
}

export default DeleteDeveloper;