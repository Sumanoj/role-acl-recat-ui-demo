

import React, { Component } from 'react'
import {addItem, deleteItem} from '../Redux/AddAction';
import {connect} from 'react-redux';

class ToDoList extends Component{
    constructor(props){
        debugger
        super(props)
        this.state={
            input:''
        }
    }
    handleChange=(e)=>{
        this.setState({input:e.target.value})
    }
    handleDelete=(value)=>{
    this.props.deleteItem(value)        
    }
    
    
    
    render(){
        debugger
        return(
<div>
        <h1>ToDOList Example</h1>
        <input type="text" value={this.state.input} onChange={this.handleChange}/>
            <button onClick={(event) => this.props.addItem(this.state.input)}>Add Items</button> 
    
            <ul>{this.props.task.map(ele => <li key={ele}>{ele}<button onClick={() => this.handleDelete(ele)}>Delete</button></li>)}</ul>
        </div>
        )
        }
    }

   /* const mapStateToProps= state => {
        debugger
     return {
        task: state.task
     }
        
    }*/

    const mapStateToProps=state=>{
        return{
            task: state.task
        }
    }
    
    /* const test = (diapatch) => {
        return {
        addItem: (value) => {
            return diapatch({type: 'ADD_ITEM', task: value})}
    }
    } */

    const mapToDispatch=dispatch=>{
        return{
            addItem : (value)=> dispatch(addItem(value)),
            deleteItem : (value)=> dispatch(deleteItem(value))
        }
    }
        
    

export default connect(mapStateToProps, mapToDispatch)(ToDoList)