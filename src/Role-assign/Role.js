import { AccessControl } from 'role-acl';

const grantsObject = {
    admin: {
        grants: [
            {
                resource: 'article', action: ['review', 'comment', 'add', 'publish'],
            },
            {
                resource: 'video', action: ['play', 'comment', 'publish'],
            },
            {
                resource: 'audio', action: ['play', 'comment', 'publish'],
            },
            {
                resource: 'comment', action: ['read', 'add', 'delete'],
            },
            {
                resource: 'userDetails', action: ['read', 'delete']
            },

        ]
    },
    identifiedUsers: {
        grants: [
            {
                resource: 'article', action: ['comment', 'add'],
            },
            {
                resource: 'video', action: ['play', 'comment'],
            },
            {
                resource: 'audio', action: ['play', 'comment'],
            },
            {
                resource: 'comment', action: ['read', 'add'],
            },
            {
                resource: 'userDetails', action: ['view']
            },
        ]
    },
    nonIdentifiedUsers: {
        grants: [
            {
                resource: 'article', action: ['read'],
            },
            {
                resource: 'comment', action: ['read'],
            }
        ]
    },
    developer: {
        grants: [
            {
                resource: 'article', action: ['review', 'comment', 'add', 'publish'],
            },
            {
                resource: 'video', action: ['play', 'comment', 'publish'],
            },
            {
                resource: 'audio', action: ['play', 'comment', 'publish'],
            },
            {
                resource: 'comment', action: ['read', 'add', 'delete'],
            },
            {
                resource: 'userDetails', action: ['read', 'delete']
            },
        ]
    },
    contributor: {
        grants: [
            {
                resource: 'article', action: ['review', 'comment', 'add', 'publish'],
            },
            {
                resource: 'video', action: ['play'],
            },
            {
                resource: 'audio', action: ['play'],
            },
            {
                resource: 'comment', action: ['read'],
            },
            {
                resource: 'userDetails', action: ['read'],
            },
        ]
    }
}

export const ac = new AccessControl(grantsObject);

const users = [
    { uname: 'suman', password: 'test', role: 'admin' },
    { uname: 'spriha', password: 'test', role: 'developer' },
    { uname: 'manoj', password: 'test', role: 'identifiedUsers' },
    { uname: 'ravi', password: 'test', role: 'nonIdentifiedUsers' },
    { uname: 'saurabh', password: 'test', role: 'contributor' },

];
export default users;


let isAuthenticate = false;

export const login = () => {
    isAuthenticate = true;
}

export const logout = () => {
    isAuthenticate = false;
}

export const isAUthenticated = () => {
    return isAuthenticate;
}
