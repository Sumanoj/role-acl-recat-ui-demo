import React from 'react';
import {BrowserRouter as Router,Link,Switch, Route} from 'react-router-dom';
import Article from './Article';
import Video from './Video';
import Comment from './Comment'
import Audio from'./Audio';
import UserDetail from './userDetail';
import LogOut from '../Role-assign/LogOut';
import ToDoList from './todoList';

export default function Nav(props){
    let permissions = {};
    if(props.location && props.location.state){
        permissions = {...props.location.state }
    }else if(localStorage.getItem('permissions')){
        permissions = JSON.parse(localStorage.getItem('permissions'))
    }
  
if(!localStorage.getItem('isLoggedIn')){
    props.history.push('/login');
}
if(Object.keys(permissions).length === 0){
return '';
}
const  {
    articlePermissions, 
    videoPermission, 
    audioPermission, 
    commentPermission, 
    userDetailsPermission} = permissions
    let isArticlePermission=articlePermissions && articlePermissions.length > 0; 
    let isVideoPermission= videoPermission && videoPermission.length>0;
    let isAudioPermission= audioPermission && audioPermission.length>0;
    let isCommentPermission= commentPermission && commentPermission.length>0;
    let isuserDetailsPermission= userDetailsPermission && userDetailsPermission.length>0;
    return(
        <div>
        <Router>
            <ul>
                     {isArticlePermission && <li>
                         <Link to="/navigation/article">Article</Link>
                     </li>   }
                 

                {isVideoPermission && <li>
                <Link to="/navigation/video">Video</Link>
                </li> }
                {isAudioPermission &&<li>
                <Link to="/navigation/comment">Comment</Link>
                </li>}
                {isCommentPermission &&<li>
                <Link to="/navigation/audio">Audio</Link>
                </li>}
                {isuserDetailsPermission &&<li>
                <Link to="/navigation/userdetail">UserDetail</Link>
                </li>}
            </ul>
            <Switch>
                <Route exact path='/navigation/article' render={(props) => (<Article accessButton={articlePermissions} {...props}></Article>)}></Route>
                <Route path='/navigation/video' render={(props)=>(<Video accessButton={videoPermission} {...props}></Video>)}></Route>
                <Route path='/navigation/comment' render={(props)=>(<Comment accessButton={commentPermission} {...props}></Comment>)}></Route>
                <Route path='/navigation/audio' render={(props)=>(<Audio accessButton={audioPermission} {...props}></Audio>)}></Route>
                <Route path='/navigation/userdetail' render={(props)=>(<UserDetail accessButton={userDetailsPermission} {...props}></UserDetail>)}></Route>
                <Route path='/navigation/article/todolist' component={ToDoList}/>
            </Switch>
            </Router>

            <LogOut></LogOut>
            
            </div>
    )


    
}