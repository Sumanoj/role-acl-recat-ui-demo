import React from 'react'

import users, { login } from './Role'
import {ac} from './Role';
import ActionsPage from './ActionsPage'
import Button from '@material-ui/core/Button';
import './Login.css'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Nav from '../Role-assign/Navigation';

    

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            err:false
        }
    }
        handleChange=(event)=>{
            this.setState({username:event.target.value})
        }
        handleEvent=(event)=>{
            this.setState({password:event.target.value})
        }
        routeChange= async (e)=> {
        e.preventDefault()
        let user = users.find((ele) => ele.uname===this.state.username)
       if((user && user.uname===this.state.username)&& (user.password===this.state.password)){
        
        let articlePermissions = await ac.allowedActions({role: user.role, resource: 'article',uname:user.uname})
        let videoPermission = await ac.allowedActions({role: user.role, resource: 'video',uname:user.uname})
        let audioPermission = await ac.allowedActions({role: user.role, resource: 'audio',uname:user.uname})
        let commentPermission = await ac.allowedActions({role: user.role, resource: 'comment',uname:user.uname})
        let userDetailsPermission = await ac.allowedActions({role: user.role, resource: 'userDetails',uname:user.uname})

        console.log(articlePermissions,videoPermission)
    
        localStorage.setItem('isLoggedIn', true);
        localStorage.setItem('permissions',JSON.stringify({articlePermissions, videoPermission, audioPermission, commentPermission, userDetailsPermission}));
                    this.props.history.push({
                        pathname: '/navigation',
                        state: { role: user.role, articlePermissions,videoPermission, audioPermission,commentPermission,userDetailsPermission,uname:user.uname }
                      });
        }else{
           this.setState({err:true})
       }
       }
        
         showError = () => {
            if(this.state.err){
                return <h1>Something went wrong!!</h1>
            }else{
              return '';  
            }
        }

    render() {
        return(

            <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm">
            <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '50vh', marginTop: '10vh' }} >
        <div className="Login-Detail">
                {this.showError()}
                    <h3>Login Page</h3>
                    <div>
                    <TextField id="outlined-basic" label="Username" variant="outlined" value={this.state.username} onChange={this.handleChange}/>
                    </div>
                    <div>
                    <TextField id="outlined-basic"  type="password" label="Password" variant="outlined" value={this.state.password} onChange={this.handleEvent}/>
                    </div>
                    <div>
                    <Button variant="contained" color="primary" onClick={this.routeChange}>Login</Button> 

                    
                    </div>
            </div>
            </Typography>
            </Container>
            </React.Fragment>
        )
    }
}

export default Login;