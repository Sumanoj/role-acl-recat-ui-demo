import React from 'react';
import { Button } from '@material-ui/core';
import { getThemeProps } from '@material-ui/styles';
import {withRouter} from 'react-router-dom';

function LogOut(props){

 const handleClick = () => {
       localStorage.removeItem('isLoggedIn')
       localStorage.removeItem('permissions')
       props.history.push('/');
   }

    return(

        <button name="btnLog" onClick={handleClick}>LogOut</button>
    )
}

export default withRouter(LogOut)