import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import { Divider } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }));

export default function ActionsPage(props){

    const  {
        articlePermissions, 
        videoPermission, 
        audioPermission, 
        commentPermission, 
        userDetailsPermission} = props.location.state;

        const userRole=props.location.state.role;
        const userName=props.location.state.uname;
        console.log(userName);
        const classes = useStyles(userRole);
    

    return(
      <div className={classes.root}>
          <div className={classes.root}>
        <h3>Article</h3>
        {articlePermissions.map((ele, index) => <Button variant="contained" color="primary" key={index}>{ele}</Button>)}
        </div>
        <Divider />
        <div className={classes.root}>
        <h3>Video </h3>
        {videoPermission.map((ele,index)=><Button variant="contained" color="secondary" key={index}>{ele}</Button>)}        
        </div>
        <Divider />
        <div className={classes.root}>
        <h3>Audio</h3>
        {audioPermission.map((ele, index) => <Button variant="contained" color="default" key={index}>{ele}</Button>)}
        </div>
        <Divider />
        <div className={classes.root}>
        <h3>Comment</h3>
        {commentPermission.map((ele, index) => <Button variant="contained" color="secondary" key={index}>{ele}</Button>)}
        </div>
        <Divider/>
        <div className={classes.root}>
        <h3>UserDetails</h3>
        {userDetailsPermission.map((ele, index) => <Button variant="contained" color="primary" key={index}>{ele}</Button>)}
        </div>
<h1>Role of user is:{userRole}</h1>
<h1>Name of user is:{userName}</h1>
        
    </div>
    )
}