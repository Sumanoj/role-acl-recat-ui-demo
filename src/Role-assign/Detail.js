import React from 'react'
import Button from '@material-ui/core/Button';


class Detail extends React.Component{

    constructor(props){
        super(props);
        debugger
        this.state = {
            isAdmin : props.location.state.detail === 'admin'
        };

    }
    render(){
        return (
            <div>
                <h1>hello</h1>
                <button onClick={() => this.props.history.push("/about")} >About</button>
                <Button variant="contained">Default</Button>
<Button variant="contained" color="primary">
  Primary
</Button>
<Button variant="contained" color="secondary">
  Secondary
</Button>
{ !this.state.isAdmin && <Button variant="contained" disabled>
  Disabled
</Button>}
<Button variant="contained" color="primary" href="#contained-buttons">
  Link
</Button>

            </div>
        )
    }
}

export default Detail;