import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }));



export default function Comment(props){
    let accessButton=props.accessButton;
    let userRole=props.accessButton.role;
    const classes = useStyles(userRole);


    return(
        <div className={classes.root}>
        <div className={classes.root}>
            <h3>Comment</h3>
            {accessButton.map((ele, index) => <Button variant="contained" color="primary" key={index}>{ele}</Button>)}
            </div>
             </div>
   
    )

}