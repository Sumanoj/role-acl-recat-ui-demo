import React from 'react';

class Gorge1 extends React.Component{
    render(){
        const c={name:"suzuki",brand:"b1",model:"m1"};

        return(
            <Car info={c}></Car>
        )
    }
}
class Car extends React.Component{
    render(){
        return(
            <p>Brand of car:{this.props.info.brand}</p>
        )
    }
}
export default Gorge1;