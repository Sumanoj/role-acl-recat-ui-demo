import React from 'react';

function List(props){
    console.log(props.list);
    let getListItem = () => {
        return props.list.map((ele, index) => <li key={index}>{ele}</li>)
    }
return (
    <ul>
        {getListItem()}
    </ul>
)

}


export default List;