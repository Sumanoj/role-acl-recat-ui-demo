import React from 'react'
import {BrowserRouter,Switch,Link,Route,Redirect,useHistory,useLocation} from 'react-router-dom';

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
      fakeAuth.isAuthenticated = true;
      setTimeout(cb, 100); // fake async
    },
    signout(cb) {
      fakeAuth.isAuthenticated = false;
      setTimeout(cb, 100);
    }
  };

  function PrivateRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          fakeAuth.isAuthenticated ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }
  
function PublicPage(){
    return <h1>Public</h1>
}

function ProtectedPage(){
    return <h1>Protected</h1>

}
function LoginPage(){
    let history=useHistory();
    let location=useLocation();

    let {from} =location.state||{from:{pathname:"/"}};
    let login=()=>{
        fakeAuth.authenticate(()=>{
            history.replace(from)
        });
    };

    return(
        <div>
            <p>you must be log in to view {from.pathname}</p>
            <button onClick={login}>Login In</button>
        </div>
    )
}

export default function AuthExample(){
    return(
        
<BrowserRouter>
<div>
    <ul>
        <li>
<Link to="/public">Public</Link>
</li>
<li>
  <Link to="/protected">Protected</Link>  
</li>
</ul>

<Switch>

<Route path="/public" component={PublicPage}></Route>
<Route path="/login" component={LoginPage}></Route>
<PrivateRoute path="/protected" component={ProtectedPage}></PrivateRoute>

</Switch>
</div>

</BrowserRouter>




    )

}
