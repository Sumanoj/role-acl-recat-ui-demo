import React,{Component} from 'react';
import './voting.css';

class Voting extends Component{	
	constructor(props){
		super(props);
		this.state = {
			name : '',
			languages : [
				{name: "Php", votes: 0},
				{name: "Python", votes: 0},
				{name: "Go", votes: 0},
				{name: "Java", votes: 0}
			]
		}
	}

	handleOnChange =(event) => {
		this.setState({name: event.target.value})
	}

	addContestant = () => {
		let newLanguages = [...this.state.languages];
		newLanguages.push({
			name: this.state.name,
			votes: 0
		})
		this.setState({languages: newLanguages});
	}

	vote (i) {


		let newLanguages = [...this.state.languages];
		newLanguages[i].votes++;
		this.setState({languages: newLanguages});
	}

	render(){
		return(
			<React.Fragment>
				<input type="text" value={this.state.name} onChange={this.handleOnChange} ></input>
				<button onClick={this.addContestant}>Add Contestant</button>
				<h1>Vote Your Language!</h1>
				<div className="languages">
					{
						this.state.languages.map((lang, i) => 
							<div key={i} className="language">
								<div className="voteCount">
									{lang.votes}
								</div>
								<div className="languageName">
									{lang.name} 
								</div>
								<button onClick={this.vote.bind(this, i)}>Click Here</button>
							</div>
						)
					}
				</div>
			</React.Fragment>
		);
	}
}
export default Voting;