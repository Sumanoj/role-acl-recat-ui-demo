import React from 'react';

class Votings extends React.Component{
    constructor(props){
        super(props);
        this.state={
            name:'',
            languages:[
                {name:"C#",votes:0},
                {name:"Java",votes:0},
                {name:"python",votes:0}

            ]
        }
    }
    handleOnChange=(event)=>{
        this.setState({name: event.target.value})
    }

    addConstentant=()=>{
        let newLanguages=[...this.state.languages];
        newLanguages.push({
            name:this.state.name,
            votes:0
        })
        this.setState({languages:newLanguages})

    }

    vote(i){
        let newLanguages=[...this.state.languages];
        newLanguages[i].votes++;
        this.setState({languages:newLanguages})
    }
    render(){
        return(
            <React.Fragment>
                <input type="text" value={this.state.name} onChange={this.handleOnChange}></input>
                <button type="button" onClick={this.addConstentant}>AddConstentant</button>
                <div className="languages">
                {
                    this.state.languages.map((ele,i)=>
                    <div key={i} className="language">
                        <div className="languageName">{ele.name}</div>

                        <div className="languageCount">{ele.votes}</div>
                        <button onClick={this.vote.bind(this,i)}>click me</button>
                    </div>

                    )

                }

                </div>





            </React.Fragment>

        )
    }
}
export default Votings; 