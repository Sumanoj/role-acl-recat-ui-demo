import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from './Role-assign/Login';
import Nav from './Role-assign/Navigation';
import store from './../src/Redux/Store'
import {Provider} from 'react-redux';


export default function App() {
  return (
    <Provider store={store}> 
    <div>
    <Router>
        <Switch>
           <Route path="/" exact component = {Nav}></Route>
           <Route exact path="/navigation" component={Nav}></Route>
           <Route exact path="/login" component={Login}></Route>
       </Switch>
      </Router>
      </div>

      </Provider>

   
  )
}
    