import React from 'react';

class AutoComplete extends React.Component{
    constructor(props){
        super(props);
        
        this.state={
            arr:["suman","manoj","spriha"],
            filArr:[],
            arr1:["suman","manoj","spriha"]

        }
        this.handleChange=this.handleChange.bind(this);
    }
     
    handleChange(event) {
        const filArr= this.state.arr1.filter(ele => ele.indexOf(event.target.value)>-1)
        this.setState({arr:filArr})
    }
     
    render(){
        return(

            <form>
                <label>
                    <input type="text" value={this.state.value} onChange={this.handleChange}/>
                </label>
                {this.state.arr.map(ele=><li>{ele}</li>)}
        
            </form>
        );


        
    }
}

export default AutoComplete;
